-- http://projects.haskell.org/xmobar/
-- install xmobar with these flags: --flags="with_alsa" --flags="with_mpd" --flags="with_xft"  OR --flags="all_extensions"
-- you can find eather location codes here: http://weather.noaa.gov/index.html

Config { font    = "xft:Ubuntu:weight=bold:pixelsize=14"
       , additionalFonts = [ "xft:Mononoki Nerd Font:pixelsize=14"
                           , "xft:Font Awesome 5 Free Solid:pixelsize=14"
                           , "xft:Font Awesome 5 Brands:pixelsize=14"
                           ]
       , bgColor = "#282c36"
       , fgColor = "#ff6e6e"
       , position = Static { xpos = 0 , ypos = 0, width = 2560, height = 26}
       , lowerOnStart = True
       , hideOnStart = False
       , allDesktops = True
       , persistent = True
       , iconRoot = ".xmonad/xpm/"
       , commands = [
                      -- Time and date
                      Run Date "<fn=1>\xf133 </fn>  %b %d %Y - (%H:%M) " "date" 50
                      -- Network up and down
                    , Run Network "enp6s0" ["-t", "<fn=1>\xf0ab</fn>  <rx>kb  <fn=1>\xf0aa</fn>  <tx>kb"] 20
                      -- Cpu usage in percent
                    , Run Cpu ["-t", " (<total>%)","-H","50","--high","red"] 20
                    , Run CpuFreq["-t", "5950x: <cpu0>GHz","-H","50","--high","red","--ddigits","1"] 10
                    , Run MultiCoreTemp["-t", "<avg>°C","-H","80","--high","red"] 50
                      -- Ram used number and percent
                    , Run Memory ["-t", "<fn=1>\xf233 </fn>  mem: <used> MB","--width","5"] 20
                      -- Disk space free
                    , Run DiskU [("/", "<fn=1>\xf0c7 </fn><free> free")] [] 60
                    , Run Com ".local/bin/gpupower" [] "gpupower" 20
                    , Run Com ".local/bin/gputemp" [] "gputemp" 20
                      -- Runs custom script to check for pacman updates.
                      -- This script is in my dotfiles repo in .local/bin.
                    , Run Com ".local/bin/pacupdate" [] "pacupdate" 36000
                    , Run Weather "KOKC"
                                    ["-t", "<skyCondition>: <tempF>°F"
                                    , "-L","50", "-H", "95"
                                    , "--high", "lightgoldenrod4", "--low", "babyblue"] 18000

                      -- Runs a standard shell command 'uname -r' to get kernel version
                    , Run Com "uname" ["-r"] "" 3600
                      -- Prints out the left side items such as workspaces, layout, etc.
                      -- The workspaces are 'clickable' in my configs.
                    , Run UnsafeStdinReader
                    ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = " <action=`xdotool key control+alt+g`><icon=haskell_20.xpm/> </action><fc=#6272a4>  |</fc> %UnsafeStdinReader% }{  <box type=Bottom width=2 mb=2 color=#bd93f9><fc=#bd93f9> %disku% </fc> </box><fc=#6272a4> |</fc> <box type=Bottom width=2 mb=2 color=#ff6e6e><fc=#ff6e6e> %memory% </fc></box><fc=#6272a4> | </fc><box type=Bottom width=2 mb=2 color=#ffb86c> <fc=#ffb86c> <fn=1>  </fn>%cpufreq% %cpu% </fc><fc=#ffffa5><fn=1> </fn> %multicoretemp% </fc></box><fc=#6272a4> |</fc> <box type=Bottom width=2 mb=2 color=#ff79c6> <fc=#ff79c6><fn=1> </fn> 6900xt: %gpupower%</fc><fc=#51afef> <fn=1> </fn> %gputemp%°C</fc></box><fc=#8be9fd><fc=#6272a4> | </fc><box type=Bottom width=2 mb=2 color=#8be9fd><fn=1> </fn> %uname% </box></fc>"
       }
