#  __     __                    __   __
# |   \  |  |                  |__| |__|
# |    \ |  |   ___    __  _    __   __
# |     \|  |  /   \  \  \/ /  |  | |  |
# |  |\     | |  |  |  \  \/   |  | |  |
# |  | \    | |  |  |  /\  \   |  | |  |
# |__|  \___|  \___/  /_/\__|  |__| |__|
#
#
set -e fish_user_paths
set -U fish_user_paths $fish_user_paths $HOME/.local/bin/ $HOME/Applications

set fish_greeting                      # Supresses fish's intro message

# Dracula Color Palette
set -l foreground f8f8f2
set -l selection 44475a
set -l comment 6272a4
set -l red ff5555
set -l orange ffb86c
set -l yellow f1fa8c
set -l green 50fa7b
set -l purple bd93f9
set -l cyan 8be9fd
set -l pink ff79c6

# Syntax Highlighting Colors
set -g fish_color_normal $foreground
set -g fish_color_command $cyan
set -g fish_color_keyword $pink
set -g fish_color_quote $yellow
set -g fish_color_redirection $foreground
set -g fish_color_end $orange
set -g fish_color_error $red
set -g fish_color_param $purple
set -g fish_color_comment $comment
set -g fish_color_selection --background=$selection
set -g fish_color_search_match --background=$selection
set -g fish_color_operator $green
set -g fish_color_escape $pink
set -g fish_color_autosuggestion $comment

# Completion Pager Colors
set -g fish_pager_color_progress $comment
set -g fish_pager_color_prefix $cyan
set -g fish_pager_color_completion $foreground
set -g fish_pager_color_description $comment

set TERM "xterm-256color"              # Sets the terminal type
#set EDITOR "emacsclient -t -a ''"      # $EDITOR use Emacs in terminal
set EDITOR "nvim"
set VISUAL "emacsclient -c -a emacs"   # $VISUAL use Emacs in GUI mode

set -x MANPAGER "sh -c 'col -bx | bat -l man -p'" 
alias cat='bat'

### DEFAULT EMACS MODE OR VI MODE ###
function fish_user_key_bindings
#  fish_default_key_bindings
  fish_vi_key_bindings
end
### END OF VI MODE ###
###
###
###
### END OF SPARK ###
set -g spark_version 1.0.0

complete -xc spark -n __fish_use_subcommand -a --help -d "Show usage help"
complete -xc spark -n __fish_use_subcommand -a --version -d "$spark_version"
complete -xc spark -n __fish_use_subcommand -a --min -d "Minimum range value"
complete -xc spark -n __fish_use_subcommand -a --max -d "Maximum range value"

function spark -d "sparkline generator"
    if isatty
        switch "$argv"
            case {,-}-v{ersion,}
                echo "spark version $spark_version"
            case {,-}-h{elp,}
                echo "usage: spark [--min=<n> --max=<n>] <numbers...>  Draw sparklines"
                echo "examples:"
                echo "       spark 1 2 3 4"
                echo "       seq 100 | sort -R | spark"
                echo "       awk \\\$0=length spark.fish | spark"
            case \*
                echo $argv | spark $argv
        end
        return
    end

    command awk -v FS="[[:space:],]*" -v argv="$argv" '
        BEGIN {
            min = match(argv, /--min=[0-9]+/) ? substr(argv, RSTART + 6, RLENGTH - 6) + 0 : ""
            max = match(argv, /--max=[0-9]+/) ? substr(argv, RSTART + 6, RLENGTH - 6) + 0 : ""
        }
        {
            for (i = j = 1; i <= NF; i++) {
                if ($i ~ /^--/) continue
                if ($i !~ /^-?[0-9]/) data[count + j++] = ""
                else {
                    v = data[count + j++] = int($i)
                    if (max == "" && min == "") max = min = v
                    if (max < v) max = v
                    if (min > v ) min = v
                }
            }
            count += j - 1
        }
        END {
            n = split(min == max && max ? "▅ ▅" : "▁ ▂ ▃ ▄ ▅ ▆ ▇ █", blocks, " ")
            scale = (scale = int(256 * (max - min) / (n - 1))) ? scale : 1
            for (i = 1; i <= count; i++)
                out = out (data[i] == "" ? " " : blocks[idx = int(256 * (data[i] - min) / scale) + 1])
            print out
        }
    '
end
### END OF SPARK ###

### FUNCTIONS ###
# Spark functions
function letters
    cat $argv | awk -vFS='' '{for(i=1;i<=NF;i++){ if($i~/[a-zA-Z]/) { w[tolower($i)]++} } }END{for(i in w) print i,w[i]}' | sort | cut -c 3- | spark | lolcat
    printf  '%s\n' 'abcdefghijklmnopqrstuvwxyz'  ' ' | lolcat
end


# Function for creating a backup file
# ex: backup file.txt
# result: copies file as file.txt.bak
function backup --argument filename
    cp $filename $filename.bak
end

# Function for copying files and directories, even recursively.
# ex: copy DIRNAME LOCATIONS
# result: copies the directory and all of its contents.
function copy
    set count (count $argv | tr -d \n)
    if test "$count" = 2; and test -d "$argv[1]"
	set from (echo $argv[1] | trim-right /)
	set to (echo $argv[2])
        command cp -r $from $to
    else
        command cp $argv
    end
end

# Function for printing a column (splits input on whitespace)
# ex: echo 1 2 3 | coln 3
# output: 3
function coln
    while read -l input
        echo $input | awk '{print $'$argv[1]'}'
    end
end

# Function for printing a row
# ex: seq 3 | rown 3
# output: 3
function rown --argument index
    sed -n "$index p"
end

# Function for ignoring the first 'n' lines
# ex: seq 10 | skip 5
# results: prints everything but the first 5 lines
function skip --argument n
    tail +(math 1 + $n)
end

# Function for taking the first 'n' lines
# ex: seq 10 | take 5
# results: prints only the first 5 lines
function take --argument number
    head -$number
end
### END OF FUNCTIONS ###

# Function for org-agenda
function org-search -d "send a search string to org-mode"
    set -l output (/usr/bin/emacsclient -a "" -e "(message \"%s\" (mapconcat #'substring-no-properties \
        (mapcar #'org-link-display-format \
        (org-ql-query \
        :select #'org-get-heading \
        :from  (org-agenda-files) \
        :where (org-ql--query-string-to-sexp \"$argv\"))) \
        \"
    \"))")
    printf $output
end

# Spark
alias cl='clear; echo; echo; seq 1 (tput cols) | sort -R | spark | lolcat; echo; echo'

### ALIASES ###

alias monthlybudget="ledger -f ~/Documents/MEGAsync/Ledger/monthly.dat -f ~/Documents/MEGAsync/Ledger/purchases.dat --monthly bal ^assets ^liabilities"
alias back="~/.local/bin/backcam.sh"
alias front="~/.local/bin/frontcam.sh"
alias matlab="/drives/tbSSD/Applications/Matlab/bin/matlab"
alias wp="/drives/tbSSD/Applications/Cloning/styli.sh/styli.sh -s nature"
alias cad="/drives/tbSSD/Applications/Cloning/Fusion-360---Linux-Wine-Version-/scripts/fusion360-start.sh"
alias kp="keepassxc-cli show -s /drives/tbSSD/Applications/Cloning/arconoxii/Passwords.kbdx"
alias dc="expressvpn disconnect"
alias vpn="expressvpn connect smart"
alias grep="rg --sort path --ignore-case"
alias grepcase="rg --sort path"
alias srb="sudo systemctl reboot --firmware-setup"
alias fanon="sudo amdgpu-fan"
alias ipscan="sudo arp-scan -l"
alias top="btop"
#alias emacs='devour emacs'
alias xmon='emacs ~/.xmonad/xmonad.hs'
alias xmob='emacs ~/.config/xmobar/'
alias vimf='nvim +CocCommand explorer'
#alias emacs="devour emacsclient -c -a 'emacs'"
alias em="devour emacsclient -c -a 'emacs'"
alias afk='/opt/shell-color-scripts/colorscripts/pipes2'
alias qb='qutebrowser'
alias upxmon="sudo ghc --make ~/.xmonad/xmonad.hs -i -ilib -fforce-recomp -main-is main -dynamic -v0 -o xmonad-x86_64-linux"
alias logs="journalctl -f"
alias ufw="sudo ufw"
alias sxiv="devour sxiv"
alias mpv="devour mpv"

# Dmenu
alias configsgitlab.sh='$HOME/.config/dmenu/configsgitlab.sh'
alias wowpushgitlab.sh='$HOME/.config/dmenu/wowpushgitlab.sh'
alias backcam.sh='$HOME/.config/dmenu/backcam.sh'
alias constat.sh='$HOME/.config/dmenu/constat.sh'
alias frontcam.sh='$HOME/.config/dmenu/frontcam.sh'
alias xrandrfix='$HOME/.config/dmenu/xrandrfix'

# vim and emacs
alias vim='nvim'
alias em='/usr/bin/emacs -nw'
alias emacs="devour emacsclient -c -a 'emacs'"
alias doomsync="~/.emacs.d/bin/doom sync"
alias doomdoctor="~/.emacs.d/bin/doom doctor"
alias doomupgrade="~/.emacs.d/bin/doom upgrade"
alias doompurge="~/.emacs.d/bin/doom purge"

# root privileges
alias doas="doas --"

# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# broot
alias br='broot -hw'
alias bs='broot -hdwp'

# gitlab
alias githelp='echo "git add __ or -u (new files) > git commit -u explanation > git push"'
alias gitpush='git push git@gitlab.com:noxii/'
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias stat='git status'  # 'status' is protected name so using 'stat' instead
alias tag='git tag'
alias newtag='git tag -a'

# debugging
alias jctl="journalctl -p 3 -xb"

# Changing "ls" to "exa"
alias ll='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ls='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing

# pacman and yay
alias syu='paru -Syu --noconfirm'              # update standard pkgs and AUR pkgs
alias cleanup='rm -rf ~/.local/share/Trash; sudo pacman -Rns (pacman -Qtdq)'  # remove orphaned packages
alias failedupdate='sudo rm /var/lib/pacman/db.lck'  # fix failed update

# get fastest mirrors
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# adding flags
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias vifm='~/.config/vifm/scripts/vifmrun .'

# shutdown or reboot
alias ssn="sudo shutdown now"
alias sr="sudo reboot"
alias srb="sudo systemctl reboot --firmware-setup"

# gpg encryption
# verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
# receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

# youtube-dl
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "
alias ytv-best="youtube-dl -f bestvideo+bestaudio "

# switch between shells
# I do not recommend switching default SHELL from bash.
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"
alias tofish="sudo chsh $USER -s /bin/fish && echo 'Now log out.'"

### RANDOM COLOR SCRIPT ###
# Get this script from my GitLab: gitlab.com/dwt1/shell-color-scripts
# Or install it from the Arch User Repository: shell-color-scripts
#colorscript random
neofetch --cpu_temp C
seq 1 (tput cols) | sort -R | spark | lolcat
echo;

alias config='/usr/bin/git --git-dir=/drives/tbSSD/Applications/Cloning/noxDotfiles --work-tree=$HOME'
alias configadd='config add -u'
alias configcom='config commit -m'
alias configpush='config push --set-upstream git@codeberg.org:notNoxii/noxDotfiles.git master'
alias crm='config rm --cached'
alias wow='/usr/bin/git --git-dir=/drives/tbSSD/Applications/Cloning/WoWaddons --work-tree=$HOME/Games/battlenet/drive_c/Program\ Files\ \(x86\)/World\ of\ Warcraft/_retail_/WTF/Account/67098567\#1/'
alias wowpush='wow push --set-upstream git@codeberg.org:notNoxii/WoWaddons.git master'


starship init fish | source
