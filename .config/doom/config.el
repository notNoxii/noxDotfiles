(map! :leader
      (:prefix ("b". "buffer")
       :desc "List bookmarks" "L" #'list-bookmarks
       :desc "Save current bookmarks to bookmark file" "w" #'bookmark-save))

(evil-define-key 'normal ibuffer-mode-map
  (kbd "f c") 'ibuffer-filter-by-content
  (kbd "f d") 'ibuffer-filter-by-directory
  (kbd "f f") 'ibuffer-filter-by-filename
  (kbd "f m") 'ibuffer-filter-by-mode
  (kbd "f n") 'ibuffer-filter-by-name
  (kbd "f x") 'ibuffer-filter-disable
  (kbd "g h") 'ibuffer-do-kill-lines
  (kbd "g H") 'ibuffer-update)

;; https://stackoverflow.com/questions/9547912/emacs-calendar-show-more-than-3-months
(defun dt/year-calendar (&optional year)
  (interactive)
  (require 'calendar)
  (let* (
      (current-year (number-to-string (nth 5 (decode-time (current-time)))))
      (month 0)
      (year (if year year (string-to-number (format-time-string "%Y" (current-time))))))
    (switch-to-buffer (get-buffer-create calendar-buffer))
    (when (not (eq major-mode 'calendar-mode))
      (calendar-mode))
    (setq displayed-month month)
    (setq displayed-year year)
    (setq buffer-read-only nil)
    (erase-buffer)
    ;; horizontal rows
    (dotimes (j 4)
      ;; vertical columns
      (dotimes (i 3)
        (calendar-generate-month
          (setq month (+ month 1))
          year
          ;; indentation / spacing between months
          (+ 5 (* 25 i))))
      (goto-char (point-max))
      (insert (make-string (- 10 (count-lines (point-min) (point-max))) ?\n))
      (widen)
      (goto-char (point-max))
      (narrow-to-region (point-max) (point-max)))
    (widen)
    (goto-char (point-min))
    (setq buffer-read-only t)))

(defun dt/scroll-year-calendar-forward (&optional arg event)
  "Scroll the yearly calendar by year in a forward direction."
  (interactive (list (prefix-numeric-value current-prefix-arg)
                     last-nonmenu-event))
  (unless arg (setq arg 0))
  (save-selected-window
    (if (setq event (event-start event)) (select-window (posn-window event)))
    (unless (zerop arg)
      (let* (
              (year (+ displayed-year arg)))
        (dt/year-calendar year)))
    (goto-char (point-min))
    (run-hooks 'calendar-move-hook)))

(defun dt/scroll-year-calendar-backward (&optional arg event)
  "Scroll the yearly calendar by year in a backward direction."
  (interactive (list (prefix-numeric-value current-prefix-arg)
                     last-nonmenu-event))
  (dt/scroll-year-calendar-forward (- (or arg 1)) event))

(map! :leader
      :desc "Scroll year calendar backward" "<left>" #'dt/scroll-year-calendar-backward
      :desc "Scroll year calendar forward" "<right>" #'dt/scroll-year-calendar-forward)

(defalias 'year-calendar 'dt/year-calendar)

(setq holiday-local-holidays
     '((holiday-fixed 7 14 "Bastille Day"))
      )

(use-package! calfw)
(use-package! calfw-org)

(use-package dashboard
  :init      ;; tweak dashboard config before loading it
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-banner-logo-title "\nKEYBINDINGS:\
\nFind file               (SPC .)     \
Open buffer list    (SPC b i)\
\nFind recent files       (SPC f r)   \
Open the eshell     (SPC e s)\
\nOpen dired file manager (SPC d d)   \
List of keybindings (SPC h b b)")
  ;;(setq dashboard-startup-banner 'logo) ;; use standard emacs logo as banner
  (setq dashboard-startup-banner "~/.config/doom/doom-emacs-dash.png")  ;; use custom image as banner
  (setq dashboard-center-content nil) ;; set to 't' for centered content
  (setq dashboard-items '((recents . 5)
                          (agenda . 5 )
                          (bookmarks . 5)
                          (projects . 5)
                          (registers . 5)))
  (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
  :config
  (dashboard-setup-startup-hook)
  (dashboard-modify-heading-icons '((recents . "file-text")
                                    (bookmarks . "book"))))

(setq doom-fallback-buffer "*dashboard*")

(map! :leader
      (:prefix ("d" . "dired")
       :desc "Open dired" "d" #'dired
       :desc "Dired jump to current" "j" #'dired-jump)
      (:after dired
       (:map dired-mode-map
        :desc "Peep-dired image previews" "d p" #'peep-dired
        :desc "Dired view file" "d v" #'dired-view-file)))

(evil-define-key 'normal dired-mode-map
  (kbd "M-RET") 'dired-display-file
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-open-file ; use dired-find-file instead of dired-open.
  (kbd "m") 'dired-mark
  (kbd "t") 'dired-toggle-marks
  (kbd "u") 'dired-unmark
  (kbd "C") 'dired-do-copy
  (kbd "D") 'dired-do-delete
  (kbd "J") 'dired-goto-file
  (kbd "M") 'dired-do-chmod
  (kbd "O") 'dired-do-chown
  (kbd "P") 'dired-do-print
  (kbd "R") 'dired-do-rename
  (kbd "T") 'dired-do-touch
  (kbd "Y") 'dired-copy-filenamecopy-filename-as-kill ; copies filename to kill ring.
  (kbd "+") 'dired-create-directory
  (kbd "-") 'dired-up-directory
  (kbd "% l") 'dired-downcase
  (kbd "% u") 'dired-upcase
  (kbd "; d") 'epa-dired-do-decrypt
  (kbd "; e") 'epa-dired-do-encrypt)
;; Get file icons in dired
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;; With dired-open plugin, you can launch external programs for certain extensions
;; For example, I set all .png files to open in 'nsxiv' and all .mp4 files to open in 'mpv'
(setq dired-open-extensions '(("gif" . "nsxiv -a")
                              ("jpg" . "nsxiv")
                              ("png" . "nsxiv")
                              ("mkv" . "mpv")
                              ("mp4" . "mpv")))

(evil-define-key 'normal peep-dired-mode-map
  (kbd "j") 'peep-dired-next-file
  (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)

(setq delete-by-moving-to-trash t
      trash-directory "~/.local/share/Trash/files/")

(setq doom-theme 'doom-dracula)
(map! :leader
      :desc "Load new theme"
      "h t" #'counsel-load-theme)

(use-package! elfeed-goodies)
(elfeed-goodies/setup)
(setq elfeed-goodies/entry-pane-size 0.5)
(add-hook 'elfeed-show-mode-hook 'visual-line-mode)
(setq-default elfeed-search-filter "@2-day-ago")
(evil-define-key 'normal elfeed-show-mode-map
  (kbd "j") 'elfeed-goodies/split-show-next
  (kbd "k") 'elfeed-goodies/split-show-prev)
(evil-define-key 'normal elfeed-search-mode-map
  (kbd "j") 'elfeed-goodies/split-show-next
  (kbd "k") 'elfeed-goodies/split-show-prev)

(require 'elfeed)
(defun elfeed-play-with-mpv ()
  "Play entry link with mpv."
  (interactive)
  (let ((entry (if (eq major-mode 'elfeed-show-mode) elfeed-show-entry (elfeed-search-selected :single)))
        (quality-arg "")
        )
    (message "Opening %s with mpv..." (elfeed-entry-link entry))
    (start-process "elfeed-mpv" nil "mpv" (elfeed-entry-link entry))))

(defvar elfeed-mpv-patterns
  '("odysee"
    "twitch"
    "invidious")
  "List of regexp to match against elfeed entry link to know
whether to use mpv to visit the link.")

(setq elfeed-feeds (quote(
                     ("https://invidious.snopyta.org/feed/channel/UCXuqSBlHAE6Xw-yeJA0Tunw" Youtube)   ;Linus
                     ("https://invidious.snopyta.org/feed/channel/UC70SrI3VkT1MXALRtf0pcHg" Youtube)    ;Thomas Delauer
                     ("https://invidious.snopyta.org/feed/channel/UCkWQ0gDrqOCarmUKmppD7GQ" Youtube)    ;JayzTwoCents
                     ("https://invidious.snopyta.org/feed/channel/UC-lHJZR3Gqxm24_Vd_AJ5Yw" Youtube)    ;PewDiePie
                     ("https://invidious.snopyta.org/feed/channel/UCws8RYvQRYssCFNoupp4zmA" Youtube)    ;FoxTailWhipz
                     ("https://invidious.snopyta.org/feed/channel/UCX34tk-noBVC4WVC9qQGyMw" Youtube)    ;Nobbel
                     ("https://invidious.snopyta.org/feed/channel/UChIs72whgZI9w6d6FhwGGHA" Youtube)    ;Gamers Nexus
                     ("https://invidious.snopyta.org/feed/channel/UCgeS8J9W62B-uLBgzp14BtQ" Youtube)    ;GN2
                     ("https://invidious.snopyta.org/feed/channel/UCeeFfhMcJa1kjtfZAGskOCA" Youtube)   ;LTT Sideshow
                     ("https://invidious.snopyta.org/feed/channel/UCJvDEmKLft6F2MxhuNUMwag" Youtube)   ;Legalbytes
                     ("https://odysee.com/$/rss/@RekietaLaw:a" Odysee)
                     ("https://odysee.com/$/rss/@DistroTube:2" Odysee)
                     ("https://odysee.com/$/rss/@timcast:c" Odysee)
                     ("https://odysee.com/$/rss/@TimcastNews:0" Odysee)
                     ("https://odysee.com/$/rss/@BrodieRobertson:5" Odysee)
                     ("https://bibliogram.art/u/joerogan/" Instagram)
                     ("https://twitchrss.appspot.com/vod/asmongold" Twitch)
                     ("https://twitchrss.appspot.com/vod/maximum" Twitch)
                     ("https://twitchrss.appspot.com/vod/driney_" Twitch)
                     ("https://invidious.snopyta.org/feed/channel/UCadiU6WTKl65HUwEih1XLYg" Youtube) ; picturefit
                     ("https://invidious.snopyta.org/feed/channel/UCe0DNp0mKMqrYVaTundyr9w" Youtube) ; vaatividya
                     ("https://invidious.snopyta.org/feed/channel/UCBJycsmduvYEL83R_U4JriQ" Youtube)))) ;Marques Brownlee

(custom-set-faces
 '(elfeed-search-date-face
   ((t  :foreground "#f0f"
        :weight extra-bold
        :underline t))))

(defface relevant-elfeed-entry
  '((t :foreground "#bd93f9"))
  "Marks an relevant Elfeed entry.")
(push '(Twitch relevant-elfeed-entry)
      elfeed-search-face-alist)

(defface ody-elfeed-entry
  '((t :foreground "#50fa7b"))
  "Marks an relevant Elfeed entry.")
(push '(Odysee ody-elfeed-entry)
      elfeed-search-face-alist)

(defface yt-elfeed-entry
  '((t :foreground "#ff5555"))
  "Marks an relevant Elfeed entry.")
(push '(Youtube yt-elfeed-entry)
      elfeed-search-face-alist)

(use-package emojify
  :hook (after-init . global-emojify-mode))

(setq doom-font (font-spec :family "Ubuntu Mono" :size 20)
      doom-variable-pitch-font (font-spec :family "Source Code Pro" :size 18)
      doom-big-font (font-spec :family "Source Code Pro" :size 20))
      doom-serif-font (font-spec :family "Source Code Pro" :size 18)
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :weight bold)
  '(font-lock-keyword-face :slant italic))

(defun dt/insert-todays-date (prefix)
  (interactive "P")
  (let ((format (cond
                 ((not prefix) "%A, %B %d, %Y")
                 ((equal prefix '(4)) "%m-%d-%Y")
                 ((equal prefix '(16)) "%Y-%m-%d"))))
    (insert (format-time-string format))))

(require 'calendar)
(defun dt/insert-any-date (date)
  "Insert DATE using the current locale."
  (interactive (list (calendar-read-date)))
  (insert (calendar-date-string date)))

(map! :leader
      (:prefix ("i d" . "Insert date")
        :desc "Insert any date" "a" #'dt/insert-any-date
        :desc "Insert todays date" "t" #'dt/insert-todays-date))

(require 'ivy-posframe)
(setq ivy-posframe-display-functions-alist
      '((swiper                     . ivy-posframe-display-at-point)
        (complete-symbol            . ivy-posframe-display-at-point)
        (counsel-M-x                . ivy-display-function-fallback)
        (counsel-esh-history        . ivy-posframe-display-at-window-center)
        (counsel-describe-function  . ivy-display-function-fallback)
        (counsel-describe-variable  . ivy-display-function-fallback)
        (counsel-find-file          . ivy-display-function-fallback)
        (counsel-recentf            . ivy-display-function-fallback)
        (counsel-register           . ivy-posframe-display-at-frame-bottom-window-center)
        (dmenu                      . ivy-posframe-display-at-frame-top-center)
        (nil                        . ivy-posframe-display))
      ivy-posframe-height-alist
      '((swiper . 20)
        (dmenu . 20)
        (t . 10)))
(ivy-posframe-mode 1) ; 1 enables posframe-mode, 0 disables it.

(map! :leader
      (:prefix ("v" . "Ivy")
       :desc "Ivy push view" "v p" #'ivy-push-view
       :desc "Ivy switch view" "v s" #'ivy-switch-view))

(autoload 'ledger-mode "ledger-mode" "A major mode for Ledger" t)
(add-to-list 'load-path
             (expand-file-name "~/Documents/MEGAsync/Ledger/"))
(add-to-list 'auto-mode-alist '("\\.ledger$" . ledger-mode)
             (add-hook 'ledger-mode-hook
          (lambda ()
            (setq-local tab-always-indent 'complete)
            (setq-local completion-cycle-threshold t)
            (setq-local ledger-complete-in-steps t))))

(setq display-line-numbers-type t)
(map! :leader
      :desc "Comment or uncomment lines" "TAB TAB" #'comment-line
      (:prefix ("t" . "toggle")
       :desc "Toggle line numbers" "l" #'doom/toggle-line-numbers
       :desc "Toggle line highlight in frame" "h" #'hl-line-mode
       :desc "Toggle line highlight globally" "H" #'global-hl-line-mode
;       :desc "Toggle truncate lines" "t" #'toggle-truncate-lines))
       :desc "Pause/Start Timer" "p" #'org-timer-pause-or-continue
       :desc "Stop Timer" "k" #'org-timer-stop
       :desc "Set Timer" "t" #'org-timer-set-timer))

(set-face-attribute 'mode-line nil :font "Ubuntu Mono-13")
(setq doom-modeline-height 30     ;; sets modeline height
      doom-modeline-bar-width 5   ;; sets right bar width
      doom-modeline-persp-name t  ;; adds perspective name to modeline
      doom-modeline-persp-icon t) ;; adds folder icon next to persp name

(map! :leader
      :desc "Edit agenda file"
      "o a" #'(lambda () (interactive) (find-file "~/Documents/MEGAsync/org/agenda.org"))
      :leader
      :desc "Edit budget file"
      "o b" #'(lambda () (interactive) (find-file "~/Documents/MEGAsync/Ledger/monthly.org"))
      :leader
      :desc "Edit shopping list"
      "o s" #'(lambda () (interactive) (find-file "~/Documents/MEGAsync/org/shopping.md"))
      :leader
      :desc "Edit xmonad"
      "- x" #'(lambda () (interactive) (find-file "~/.xmonad/README.org"))
      :leader
      :desc "Edit fish"
      "- f" #'(lambda () (interactive) (find-file "~/.config/fish/config.fish")))

(map! :leader
      :desc "Org babel tangle" "m B" #'org-babel-tangle)
(after! org
  (require 'org-bullets)  ; Nicer bullets in org-mode
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
  (setq org-directory "~/Documents/MEGAsync/org"
        org-agenda-files '("~/Documents/MEGAsync/org/agenda.org")
        org-default-notes-file (expand-file-name "notes.org" org-directory)
        org-ellipsis " ▼ "
        org-superstar-headline-bullets-list '("◉" "●" "○" "◆" "●" "○" "◆")
        org-superstar-item-bullet-alist '((?+ . ?➤) (?- . ?✦)) ; changes +/- symbols in item lists
        ;; org-log-done 'time
        org-hide-emphasis-markers t
        ;; ex. of org-link-abbrev-alist in action
        ;; [[arch-wiki:Name_of_Page][Description]]
        org-link-abbrev-alist    ; This overwrites the default Doom org-link-abbrev-list
          '(("google" . "http://www.google.com/search?q=")
            ("arch-wiki" . "https://wiki.archlinux.org/index.php/")
            ("ddg" . "https://duckduckgo.com/?q=")
            ("wiki" . "https://en.wikipedia.org/wiki/"))
        org-todo-keywords        ; This overwrites the default Doom org-todo-keywords
          '((sequence
             "TODO(t)"           ; A task that is ready to be tackled
             "PROJ(p)"           ; A project that contains other tasks
             "WORK(w)"           ; Work Things, top prio
             "|"                 ; The pipe necessary to separate "active" states and "inactive" states
             "DONE(d)"           ; Task has been completed
             "CANCELLED(c)" )))) ; Task has been cancelled

(custom-set-faces
  '(org-level-1 ((t (:inherit outline-1 :height 1.2))))
  '(org-level-2 ((t (:inherit outline-2 :height 1.15))))
  '(org-level-3 ((t (:inherit outline-3 :height 1.1))))
  '(org-level-4 ((t (:inherit outline-4 :height 1.05))))
  '(org-level-5 ((t (:inherit outline-5 :height 1.0))))
)

(map! :leader
      :desc "Journal Entry" "o j" #'org-journal-new-entry
      :leader
      :desc "Journal Search" "o J" #'org-journal-search
      :leader
      :desc "Elfeed spawn mpv"
      "m p" #'elfeed-play-with-mpv
      :leader
      :desc "Elfeed spawn mpv"
      "e p" #'elfeed-play-with-mpv
      :desc "Open Elfeed"
      "e e" #'elfeed
      :leader
      :desc "Elfeed update"
      "e u" #'elfeed-update)

(use-package ox-man)
(use-package ox-gemini)

(use-package! org-journal)
(setq org-journal-dir "~/Documents/MEGAsync/org/journal/"
      org-journal-date-prefix "* "
      org-journal-time-prefix "** "
      org-journal-date-format "%B %d, %Y (%A) "
      org-journal-file-format "%Y-%m-%d.org")

(defun dt/org-babel-tangle-async (file)
  "Invoke `org-babel-tangle-file' asynchronously."
  (message "Tangling %s..." (buffer-file-name))
  (async-start
   (let ((args (list file)))
  `(lambda ()
        (require 'org)
        ;;(load "~/.emacs.d/init.el")
        (let ((start-time (current-time)))
          (apply #'org-babel-tangle-file ',args)
          (format "%.2f" (float-time (time-since start-time))))))
   (let ((message-string (format "Tangling %S completed after " file)))
     `(lambda (tangle-time)
        (message (concat ,message-string
                         (format "%s seconds" tangle-time)))))))

(defun dt/org-babel-tangle-current-buffer-async ()
  "Tangle current buffer asynchronously."
  (dt/org-babel-tangle-async (buffer-file-name)))

(define-globalized-minor-mode global-rainbow-mode rainbow-mode
  (lambda () (rainbow-mode 1)))
(global-rainbow-mode 1 )

(map! :leader
      (:prefix ("r" . "registers")
       :desc "Copy to register" "c" #'copy-to-register
       :desc "Frameset to register" "f" #'frameset-to-register
       :desc "Insert contents of register" "i" #'insert-register
       :desc "Jump to register" "j" #'jump-to-register
       :desc "List registers" "l" #'list-registers
       :desc "Number to register" "n" #'number-to-register
       :desc "Interactively choose a register" "r" #'counsel-register
       :desc "View a register" "v" #'view-register
       :desc "Window configuration to register" "w" #'window-configuration-to-register
       :desc "Increment register" "+" #'increment-register
       :desc "Point to register" "SPC" #'point-to-register))

(setq shell-file-name "/bin/fish"
      eshell-aliases-file "~/.config/doom/aliases"
      eshell-history-size 5000
      eshell-buffer-maximum-lines 5000
      eshell-hist-ignoredups t
      eshell-scroll-to-bottom-on-input t
      eshell-destroy-buffer-when-process-dies t
      eshell-visual-commands'("bash" "fish" "htop" "ssh" "zsh")
      vterm-max-scrollback 5000)
(map! :leader
      :desc "Vterm" "e s" #'vterm
      :desc "Vterm popup toggle" "e t" #'+vterm/toggle
      :desc "Counsel eshell history" "e h" #'counsel-esh-history)

(defun prefer-horizontal-split ()
  (set-variable 'split-height-threshold nil t)
  (set-variable 'split-width-threshold 40 t)) ; make this as low as needed
(add-hook 'markdown-mode-hook 'prefer-horizontal-split)
(map! :leader
      :desc "Clone indirect buffer other window"
      "b c" #'clone-indirect-buffer-other-window)

(map! :leader
      (:prefix ("w" . "window")
       :desc "Winner redo" "<right>" #'winner-redo
       :desc "Winner undo" "<left>" #'winner-undo))
