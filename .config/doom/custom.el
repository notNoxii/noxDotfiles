(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(elfeed-goodies/entry-pane-position 'top)
 '(elfeed-goodies/entry-pane-size 0.3)
 '(package-selected-packages
   '(flycheck-ledger auto-complete vterm ssh-config-mode ssh sqlup-mode pdf-tools orglink org-journal-list org-journal org-gcal org-bullets mw-thesaurus mixed-pitch matlab-mode git-commit fontawesome flymake-shellcheck flymake-hlint evil-ledger eterm-256color eshell-syntax-highlighting eshell-outline elfeed-web elfeed-goodies elfeed-autotag dracula-theme doom-themes dired-open dictionary calfw-ical all-the-icons))
 '(warning-suppress-types '((org-element-cache) (org-element-cache))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(elfeed-search-date-face ((t :foreground "#f0f" :weight extra-bold :underline t)))
 '(font-lock-comment-face ((t (:weight bold))))
 '(font-lock-keyword-face ((t (:slant italic))))
 '(org-level-1 ((t (:inherit outline-1 :height 1.2))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.15))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.1))))
 '(org-level-4 ((t (:inherit outline-4 :height 1.05))))
 '(org-level-5 ((t (:inherit outline-5 :height 1.0)))))
(put 'customize-group 'disabled nil)
