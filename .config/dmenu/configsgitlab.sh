#!/bin/bash

git --git-dir=/drives/tbSSD/Applications/Cloning/noxDotfiles --work-tree="$HOME" add -u
rofi -dmenu -p "dotfiles" -m HDMI-A-0 | git --git-dir=/drives/tbSSD/Applications/Cloning/noxDotfiles --work-tree="$HOME" commit -F -
git --git-dir=/drives/tbSSD/Applications/Cloning/noxDotfiles --work-tree="$HOME" push --set-upstream git@codeberg.org:notNoxii/noxDotfiles.git master
