// ==UserScript==
// @name           Instagram to Nitter redirector
// @namespace      Noxii's userscripts
// @match          http://instagram.com/*
// @match          https://instagram.com/*
// @match          http://www.instagram.com/*
// @match          https://www.instagram.com/*
// @run-at         document-start
// ==/UserScript==

location.href=location.href.replace("instagram.com","bibliogram.art/u");
