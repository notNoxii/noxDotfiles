// ==UserScript==
// @name           Reddit to Nitter redirector
// @namespace      Noxii's userscripts
// @match          http://reddit.com/*
// @match          https://reddit.com/*
// @match          http://www.reddit.com/*
// @match          https://www.reddit.com/*
// @run-at         document-start
// ==/UserScript==

location.href=location.href.replace("reddit.com","teddit.net");
