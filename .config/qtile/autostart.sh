#! /bin/bash 
lxsession &
picom --experimental-backends &
nitrogen --restore &
urxvtd -q -o -f &
/usr/bin/emacs --daemon &
#volumeicon &
#nm-applet &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
