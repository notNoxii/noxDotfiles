# -*- coding: utf-8 -*-
import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from typing import List  # noqa: F401

mod = "mod4"                                     # Sets mod key to SUPER/WINDOWS
alt = "mod1"                                     # Sets mod key to
myTerm = "alacritty"                             # My terminal of choice

keys = [
         ### The essentials
         Key([mod], "Return",
             lazy.spawn(myTerm+" -e fish"),
             desc='Launches My Terminal'
             ),
         Key([mod, "shift"], "h",
             lazy.spawn("termite -e bashtop"),
             # lazy.spawn("rofi -show drun -config ~/.config/rofi/themes/dt-dmenu.rasi -display-drun \"Run: \" -drun-display-format \"{name}\""),
             desc='Task list'
             ),
         Key([mod, "shift"], "p",
             lazy.spawn("termite -e pulsemixer"),
             # lazy.spawn("rofi -show drun -config ~/.config/rofi/themes/dt-dmenu.rasi -display-drun \"Run: \" -drun-display-format \"{name}\""),
             desc='Sound Control'
             ),
         Key([mod, "shift"], "Return",
             lazy.spawn("rofi -show run"),
             # lazy.spawn("rofi -show drun -config ~/.config/rofi/themes/dt-dmenu.rasi -display-drun \"Run: \" -drun-display-format \"{name}\""),
             desc='Run Rofi'
             ),
         Key([mod, "shift"], "i",
             lazy.spawn("firefox"),
             desc='Launch Firefox'
             ),
         Key([mod, "shift"], "d",
             lazy.spawn("Discord"),
             desc='Launch Discord'
             ),
         Key([mod, "shift"], "s",
             lazy.spawn("xfce4-screenshooter"),
             desc='Screenshot'
             ),
         Key([mod, "shift"], "f",
             lazy.spawn("thunar"),
             desc='Files'
             ),
         Key([mod, "shift"], "o",
             lazy.spawn("steam --noverifyfiles"),
             desc='games'
             ),
         Key([mod, "shift"], "l",
             lazy.spawn("lutris"),
             desc='lutris'
             ),
         Key([mod, "shift"], "f",
             lazy.spawn("thunar"),
             desc='Files'
             ),
         Key([mod], "Tab",
             lazy.screen.next_group(),
             desc='Toggle through groups'
             ),
         Key([mod], "comma",
             lazy.next_layout(),
             desc='Next layout'
             ),
         Key([mod, "shift"], "c",
             lazy.window.kill(),
             desc='Kill active window'
             ),
         Key([mod, "shift"], "r",
             lazy.restart(),
             desc='Restart Qtile'
             ),
         Key([mod, "shift"], "e",
             lazy.spawn("emacs"),
             desc='Doom Emacs'
             ),
         Key([], 'XF86AudioMute',
             lazy.spawn('amixer -D pulse set Master toggle'),
             desc='change volume m '
             ),
         Key([], 'XF86AudioRaiseVolume',
             lazy.spawn('amixer -c 0 -q set Master 5%+ unmute'),
             desc='change volume + '
             ),
         Key([], 'XF86AudioLowerVolume',
             lazy.spawn('amixer -c 0 -q set Master 5%- unmute'),
             desc='change volume - '
             ),
         ### Switch focus to specific monitor (out of three)
         Key([mod], "w",
             lazy.to_screen(0),
             desc='Keyboard focus to monitor 1'
             ),
         Key([mod], "e",
             lazy.to_screen(1),
             desc='Keyboard focus to monitor 2'
             ),
         ### Switch focus of monitors
         Key([mod], "period",
             lazy.next_screen(),
             desc='Move focus to next monitor'
             ),
         ### Window controls
         Key([mod], "k",
             lazy.layout.down(),
             desc='Move focus down in current stack pane'
             ),
         Key([mod], "j",
             lazy.layout.up(),
             desc='Move focus up in current stack pane'
             ),
         Key([mod, "shift"], "k",
             lazy.layout.shuffle_down(),
             desc='Move windows down in current stack'
             ),
         Key([mod, "shift"], "j",
             lazy.layout.shuffle_up(),
             desc='Move windows up in current stack'
             ),
         Key([mod], "h",
             lazy.layout.shrink(),
             lazy.layout.decrease_nmaster(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
         Key([mod], "l",
             lazy.layout.grow(),
             lazy.layout.increase_nmaster(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
         Key([mod], "n",
             lazy.layout.normalize(),
             desc='normalize window size ratios'
             ),
         Key([mod], "m",
             lazy.layout.maximize(),
             desc='toggle window between minimum and maximum sizes'
             ),
         Key([mod], "space",
             lazy.window.toggle_fullscreen(),
             desc='toggle fullscreen'
             ),
         ### Stack controls
         Key([alt], "Tab",
             lazy.layout.next(),
             desc='Switch window focus to other pane(s) of stack'
             ),
         # Dmenu scripts launched using the key chord SUPER+p followed by 'key'
         KeyChord([mod], "p", [
             Key([], "e", lazy.spawn("./dmscripts/dmconf")),
             Key([], "i", lazy.spawn("./dmscripts/dmscrot")),
             Key([], "k", lazy.spawn("./dmscripts/dmkill")),
             Key([], "l", lazy.spawn("./dmscripts/dmlogout")),
             Key([], "m", lazy.spawn("./dmscripts/dman")),
             Key([], "r", lazy.spawn("./dmscripts/dmred")),
             Key([], "s", lazy.spawn("./dmscripts/dmsearch")),
             Key([], "p", lazy.spawn("passmenu"))
         ])
]

group_names = [("", {'layout': 'max'}),
               ("", {'layout': 'monadtall'}),
               ("", {'layout': 'monadtall'}),
               ("", {'layout': 'bsp'}),
               ("", {'layout': 'bsp'}),
               ("", {'layout': 'bsp'}),
               ("", {'layout': 'monadtall'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

layout_theme = {"border_width": 2,
                "margin": 4,
                "border_focus": "e1acff",
                "border_normal": "1D2330"
                }

layouts = [
    # layout.MonadWide(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Bsp(**layout_theme),
    # layout.Stack(stacks=2, **layout_theme),
    # layout.Columns(**layout_theme),
    # layout.RatioTile(**layout_theme),
    # layout.VerticalTile(**layout_theme),
    # layout.Matrix(**layout_theme),
    layout.Zoomy(**layout_theme),
    layout.Max(**layout_theme),
    # layout.Tile(shift_windows=True, **layout_theme),
    # layout.Stack(num_stacks=2),
    # layout.TreeTab(
    #      font = "Ubuntu",
    #      fontsize = 10,
    #      sections = ["FIRST", "SECOND"],
    #      section_fontsize = 11,
    #      bg_color = "141414",
    #      active_bg = "90C435",
    #      active_fg = "000000",
    #      inactive_bg = "384323",
    #      inactive_fg = "a0a0a0",
    #      padding_y = 5,
    #      section_top = 10,
    #      panel_width = 320
    #      ),
    # layout.Floating(**layout_theme)
]

colors = [["#282c34", "#282c34"], # panel background
          ["#3d3f4b", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#74438f", "#74438f"], # border line color for 'other tabs' and color for 'odd widgets'
          ["#4f76c7", "#4f76c7"], # color for the 'even widgets'
          ["#e1acff", "#e1acff"], # window name

# Dracula stuff (7-15)
          ["#f8f8f2", "#f8f8f2"], # white
          ["#8be9fd", "#8be9fd"], # cyan
          ["#50fa7b", "#50fa7b"], # green
          ["#ffb86c", "#ffb86c"], # orange
          ["#ff79c6", "#ff79c6"], # pink
          ["#bd93f9", "#bd93f9"], # purple
          ["#ff5555", "#ff5555"], # red
          ["#f1fa8c", "#f1fa8c"], # yellow
          ["#44475a", "#44475a"], # current line
          ["#6272a4", "#6272a4"]] # comment

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Ubuntu Bold",
    fontsize = 12,
    padding = 2,
    background=colors[0]
)
extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[2],
                       background = colors[0]
                       ),
             # widget.Image(
             #           filename = "~/.config/qtile/icons/python-white.png",
             #           scale = "False",
             #           mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm)}
             #           ),
             # widget.Sep(
             #           linewidth = 0,
             #           padding = 6,
             #           foreground = colors[2],
             #           background = colors[0]
             #           ),
              widget.GroupBox(
                       font = "Ubuntu Mono",
                       fontsize = 14,
                       margin_y = 3,
                       margin_x = 0,
                       padding_y = 5,
                       padding_x = 3,
                       borderwidth = 3,
                       active = colors[11],
                       inactive = colors[8],
                       rounded = False,
                       highlight_color = colors[1],
                       highlight_method = "line",
                       this_current_screen_border = colors[9],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[6],
                       other_screen_border = colors[4],
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.Prompt(
                       prompt = prompt,
                       font = "Ubuntu Medium",
                       padding = 10,
                       foreground = colors[3],
                       background = colors[1]
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 40,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.WindowName(
                       foreground = colors[6],
                       background = colors[0],
                       padding = 0
                       ),
              widget.Systray(
                       background = colors[0],
                       padding = 5
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[0],
                       background = colors[0]
                       ),
              widget.TextBox(
                       text = '|',
                       background = colors[0],
                       foreground = colors[15],
                       padding = 0,
                       fontsize = 20
                       ),
              widget.TextBox(
                       text = " ₿",
                       padding = 0,
                       foreground = colors[14],
                       background = colors[0],
                       fontsize = 14
                       ),
              widget.BitcoinTicker(
                       foreground = colors[14],
                       background = colors[0],
                       update_interval = 60,
                       padding = 5
                       ),
              widget.TextBox(
                       text = '|',
                       background = colors[0],
                       foreground = colors[15],
                       padding = 0,
                       fontsize = 20
                       ),
              widget.TextBox(
                       text = " 🌡",
                       padding = 2,
                       foreground = colors[13],
                       background = colors[0],
                       fontsize = 14
                       ),
              widget.ThermalSensor(
                       foreground = colors[13],
                       background = colors[0],
                       threshold = 90,
                       padding = 5,
                       tag_sensor = "junction"
                       ),
              widget.ThermalSensor(
                       foreground = colors[11],
                       background = colors[0],
                       threshold = 90,
                       padding = 5
                       ),
              widget.TextBox(
                       text='|',
                       background = colors[0],
                       foreground = colors[15],
                       padding = 0,
                       fontsize = 20
                       ),
              widget.TextBox(
                       text = " ⟳",
                       padding = 2,
                       foreground = colors[9],
                       background = colors[0],
                       fontsize = 14
                       ),
              widget.CheckUpdates(
                       update_interval = 600,
                       custom_command = "checkupdates+aur",
                       display_format = "{updates} Updates",
                       foreground = colors[9],
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' paru -Syu --noconfirm; instawow update')},
                       background = colors[0]
                       ),
              widget.TextBox(
                       text = '|',
                       background = colors[0],
                       foreground = colors[15],
                       padding = 0,
                       fontsize = 20
                       ),
              widget.TextBox(
                       text = " 🖬",
                       foreground = colors[10],
                       background = colors[0],
                       padding = 0,
                       fontsize = 16
                       ),
              widget.Memory(
                       foreground = colors[10],
                       background = colors[0],
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e bashtop')},
                       padding = 5
                       ),
              widget.TextBox(
                       text='|',
                       background = colors[0],
                       foreground = colors[15],
                       padding = 0,
                       fontsize = 20
                       ),
              # widget.Net(
              #          interface = "wlp3s0",
              #          format = '{down} ↓↑ {up}',
              #          foreground = colors[11],
              #          background = colors[0],
              #          padding = 5
              #          ),
              widget.CPU(
                       format = '{freq_current} GHz - {load_percent}%',
                       foreground = colors [11],
                       update_interval = 5
              ),
              widget.TextBox(
                       text = '|',
                       background = colors[0],
                       foreground = colors[15],
                       padding = 0,
                       fontsize = 20
                       ),
              widget.TextBox(
                      text = " Vol:",
                       foreground = colors[14],
                       background = colors[0],
                       padding = 0
                       ),
              widget.Volume(
                       foreground = colors[14],
                       background = colors[0],
                       padding = 5
                       ),
              widget.TextBox(
                       text = '|',
                       background = colors[0],
                       foreground = colors[15],
                       padding = 0,
                       fontsize = 20
                       ),
              # widget.CurrentLayoutIcon(
              #          custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
              #          foreground = colors[0],
              #          background = colors[0],
              #          padding = 0,
              #          scale = 0.7
              #          ),
              widget.CurrentLayout(
                       foreground = colors[12],
                       background = colors[0],
                       padding = 5
                       ),
              widget.TextBox(
                       text = '|',
                       background = colors[0],
                       foreground = colors[15],
                       padding = 0,
                       fontsize = 20
                       ),
              widget.OpenWeather(
                       zip = '75287',
                       metric = False,
                       format = '{main_temp}° {weather_details}',
                       update_interval = 600,
                       foreground = colors[11],
                       background = colors[0],
                       app_key = 'd217a6bc2c943310bae92acbb726cfec',
                       padding = 5
                       ),
              widget.TextBox(
                       text = '|',
                       background = colors[0],
                       foreground = colors[15],
                       padding = 0,
                       fontsize = 20
                       ),
              widget.TextBox(
                       text = "  ",
                       foreground = colors[8],
                       background = colors[0],
                       padding = 0,
                       fontsize = 16
                       ),
              widget.Clock(
                       foreground = colors[8],
                       background = colors[0],
                       format = "%B %d - %H:%M "
                       ),
              ]
    return widgets_list

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
#    del widgets_screen1[7:8]               # Slicing removes unwanted widgets (systray) on Monitors 1,3
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2                 # Monitor 2 will display all widgets in widgets_list

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=22)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=22))]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    # default_float_rules include: utility, notification, toolbar, splash, dialog,
    # file_progress, confirm, download and error.
    *layout.Floating.default_float_rules,
    Match(title='Confirmation'),  # tastyworks exit box
    Match(title='Qalculate!'),  # qalculate-gtk
    Match(title='Steam - News (1 of 2)'),  # qalculate-gtk
    Match(wm_class='kdenlive'),  # kdenlive
    Match(wm_class='pinentry-gtk-2'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
