(define-package "doom-themes" "20211221.1233" "an opinionated pack of modern color-themes"
  '((emacs "25.1")
    (cl-lib "0.5"))
  :commit "ddcbbd7298c4e506f667339b31bbf96e4abacbc9" :authors
  '(("Henrik Lissner <https://github.com/hlissner>"))
  :maintainer
  '("Henrik Lissner" . "contact@henrik.io")
  :keywords
  '("custom themes" "faces")
  :url "https://github.com/hlissner/emacs-doom-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:
