(define-package "elfeed-web" "20210226.258" "web interface to Elfeed"
  '((simple-httpd "1.5.1")
    (elfeed "3.2.0")
    (emacs "24.3"))
  :commit "162d7d545ed41c27967d108c04aa31f5a61c8e16" :authors
  '(("Christopher Wellons" . "wellons@nullprogram.com"))
  :maintainer
  '("Christopher Wellons" . "wellons@nullprogram.com")
  :url "https://github.com/skeeto/elfeed")
;; Local Variables:
;; no-byte-compile: t
;; End:
