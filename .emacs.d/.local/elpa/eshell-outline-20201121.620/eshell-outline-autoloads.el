;;; eshell-outline-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "eshell-outline" "eshell-outline.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from eshell-outline.el

(autoload 'eshell-outline-mode "eshell-outline" "\
Outline-mode in Eshell.

This is a minor mode.  If called interactively, toggle the `Eshell-Outline mode'
mode.  If the prefix argument is positive, enable the mode, and if it is zero or
negative, disable the mode.

If called from Lisp, toggle the mode if ARG is `toggle'.  Enable the mode if ARG
is nil, omitted, or is a positive number.  Disable the mode if ARG is a negative
number.

To check whether the minor mode is enabled in the current buffer, evaluate
`eshell-outline-mode'.

The mode's hook is called both when the mode is enabled and when it is disabled.

\\{eshell-outline-mode-map}

\(fn &optional ARG)" t nil)

(autoload 'eshell-outline-view-buffer "eshell-outline" "\
Clone the current eshell buffer, and enable `outline-mode'.
This will clone the buffer via `clone-indirect-buffer', so all
following changes to the original buffer will be transferred.
The command `eshell-outline-mode' offers a more interactive
version, with specialized keybindings." t nil)

(register-definition-prefixes "eshell-outline" '("eshell-outline-"))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; eshell-outline-autoloads.el ends here
