(define-package "transient" "20220508.1914" "Transient commands"
  '((emacs "25.1")
    (compat "28.1.1.0"))
  :commit "d9b3a54128065222d35ce4ba49cdb39e23a06d2f" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("extensions")
  :url "https://github.com/magit/transient")
;; Local Variables:
;; no-byte-compile: t
;; End:
