(define-package "elfeed-protocol" "20210430.846" "Provide fever/newsblur/owncloud/ttrss protocols for elfeed"
  '((emacs "24.4")
    (elfeed "2.1.1")
    (cl-lib "0.5"))
  :commit "c88bb246a40c2f8ec2cb36bc16690d1ed43f8b14" :authors
  '(("Xu Fasheng <fasheng[AT]fasheng.info>"))
  :maintainer
  '("Xu Fasheng <fasheng[AT]fasheng.info>")
  :keywords
  '("news")
  :url "https://github.com/fasheng/elfeed-protocol")
;; Local Variables:
;; no-byte-compile: t
;; End:
