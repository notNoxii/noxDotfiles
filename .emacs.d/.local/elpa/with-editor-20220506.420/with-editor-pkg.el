(define-package "with-editor" "20220506.420" "Use the Emacsclient as $EDITOR"
  '((emacs "25.1")
    (compat "28.1.1.0"))
  :commit "7348f6d5ff90318a1c948d0499d8dc6721fe851a" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("processes" "terminals")
  :url "https://github.com/magit/with-editor")
;; Local Variables:
;; no-byte-compile: t
;; End:
