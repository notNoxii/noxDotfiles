(define-package "git-commit" "20220506.1936" "Edit Git commit messages."
  '((emacs "25.1")
    (compat "28.1.1.0")
    (transient "20210920")
    (with-editor "20211001"))
  :commit "f331092df4d4dfc0a2a7424d929a9c845088d57f" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li")
    ("Sebastian Wiesner" . "lunaryorn@gmail.com")
    ("Florian Ragwitz" . "rafl@debian.org")
    ("Marius Vollmer" . "marius.vollmer@gmail.com"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("git" "tools" "vc")
  :url "https://github.com/magit/magit")
;; Local Variables:
;; no-byte-compile: t
;; End:
