(define-package "ledger-mode" "20220617.1148" "Helper code for use with the \"ledger\" command-line tool"
  '((emacs "25.1"))
  :commit "5d46822596469253507796ea3141eb824de3587a")
;; Local Variables:
;; no-byte-compile: t
;; End:
