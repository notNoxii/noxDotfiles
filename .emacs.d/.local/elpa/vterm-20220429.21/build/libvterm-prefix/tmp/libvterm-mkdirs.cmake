# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "/home/braden/.emacs.d/.local/elpa/vterm-20220429.21/build/libvterm-prefix/src/libvterm"
  "/home/braden/.emacs.d/.local/elpa/vterm-20220429.21/build/libvterm-prefix/src/libvterm-build"
  "/home/braden/.emacs.d/.local/elpa/vterm-20220429.21/build/libvterm-prefix"
  "/home/braden/.emacs.d/.local/elpa/vterm-20220429.21/build/libvterm-prefix/tmp"
  "/home/braden/.emacs.d/.local/elpa/vterm-20220429.21/build/libvterm-prefix/src/libvterm-stamp"
  "/home/braden/.emacs.d/.local/elpa/vterm-20220429.21/build/libvterm-prefix/src"
  "/home/braden/.emacs.d/.local/elpa/vterm-20220429.21/build/libvterm-prefix/src/libvterm-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "/home/braden/.emacs.d/.local/elpa/vterm-20220429.21/build/libvterm-prefix/src/libvterm-stamp/${subDir}")
endforeach()
