;;; Generated package description from org-journal.el  -*- no-byte-compile: t -*-
(define-package "org-journal" "20220103.829" "a simple org-mode based journaling mode" '((emacs "25.1") (org "9.1")) :commit "f121450610650c63aabf13afd0d2089e05fad2e4" :authors '(("Bastian Bechtold") ("Christian Schwarzgruber")) :maintainer '("Bastian Bechtold") :url "http://github.com/bastibe/org-journal")
