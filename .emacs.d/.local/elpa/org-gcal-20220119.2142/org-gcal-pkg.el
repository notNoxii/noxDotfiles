(define-package "org-gcal" "20220119.2142" "Org sync with Google Calendar"
  '((request "20190901")
    (request-deferred "20181129")
    (alert "0")
    (persist "0")
    (emacs "26")
    (org "9.3"))
  :commit "6e26ae75aea521ea5dae67e34265da534bdad2d1" :authors
  '(("myuhe <yuhei.maeda_at_gmail.com>"))
  :maintainer
  '("Raimon Grau" . "raimonster@gmail.com")
  :keywords
  '("convenience")
  :url "https://github.com/kidd/org-gcal.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
