;;; elfeed-autotag-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "elfeed-autotag" "elfeed-autotag.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from elfeed-autotag.el

(autoload 'elfeed-autotag "elfeed-autotag" "\
Setup auto-tagging rules." t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "elfeed-autotag" '("elfeed-autotag-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; elfeed-autotag-autoloads.el ends here
