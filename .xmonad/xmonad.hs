  -- Base
import XMonad
import System.Directory
import System.IO (hClose, hPutStr, hPutStrLn)
import System.Exit (exitSuccess)
import qualified XMonad.StackSet as W

    -- Actions
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (sinkAll, killAll)
import XMonad.Actions.PhysicalScreens
import qualified XMonad.Actions.Search as S

    -- Data
import Data.Char (isSpace, toUpper)
import Data.Maybe (fromJust)
import Data.Monoid
import Data.Maybe (isJust)
import Data.Tree
import qualified Data.Map as M

    -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.ManageDocks (avoidStruts, docks, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat)
import XMonad.Hooks.ServerMode
import XMonad.Hooks.SetWMName
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.WorkspaceHistory

    -- Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

    -- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders (noBorders, smartBorders)
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

   -- Utilities
import XMonad.Util.Dmenu
import XMonad.Util.EZConfig (additionalKeysP, mkNamedKeymap)
import XMonad.Util.NamedActions
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce

      -- ColorScheme module (SET ONLY ONE!)
      -- Possible choice are:
      -- DoomOne
      -- Dracula
      -- GruvboxDark
      -- MonokaiPro
      -- Nord
      -- OceanicNext
      -- Palenight
      -- SolarizedDark
      -- SolarizedLight
      -- TomorrowNight
--import Colors.Dracula

myFont :: String
myFont = "xft:SauceCodePro Nerd Font Mono:regular:size=9:antialias=true:hinting=true"

myModMask :: KeyMask
myModMask = mod4Mask        -- Sets modkey to super/windows key

myTerminal :: String
myTerminal = "kitty"    -- Sets default terminal

myBrowser :: String
myBrowser = "qutebrowser"  -- Sets qutebrowser as browser

myEmacs :: String
myEmacs = "emacsclient -c -a 'emacs' "  -- Makes emacs keybindings easier to type

myXrandr :: String
myXrandr = "~/.config/dmenu/xrandrfix"

myFrontCam :: String
myFrontCam = "~/.config/dmenu/frontcam.sh"

myBackCam :: String
myBackCam = "~/.config/dmenu/backcam.sh"

myBorderWidth :: Dimension
myBorderWidth = 1           -- Sets border width for windows

colorScheme = "dracula"

colorBack = "#282a36"
colorFore = "#f8f8f2"

color01="#000000"
color02="#ff5555"
color03="#50fa7b"
color04="#f1fa8c"
color05="#bd93f9"
color06="#ff79c6"
color07="#8be9fd"
color08="#bfbfbf"
color09="#4d4d4d"
color10="#ff6e67"
color11="#5af78e"
color12="#f4f99d"
color13="#caa9fa"
color14="#ff92d0"
color15="#9aedfe"
color16="#e6e6e6"

colorTrayer :: String
colorTrayer = "--tint 0x282a36"

myNormColor :: String
myNormColor   = color09 -- Border color of normal windows
               -- was colorBack

myFocusColor :: String
myFocusColor  = color15     -- Border color of focused windows

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myStartupHook :: X ()
myStartupHook = do
    spawnOnce "lxsession &"
    spawnOnce "picom --experimental-backends &"
    spawn myXrandr
    spawnOnce ("trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 " ++ colorTrayer ++ " --height 22")
    spawn "/usr/bin/emacs --daemon &" -- emacs daemon for the emacsclient
    spawnOnce "openrgb --gui --startminimized -c red"
    spawnOnce "liquidctl --match kraken set pump speed 90"
    spawnOnce "dunst -conf ~/.config/dunstify/dunstrc1"
    spawnOnce ("redshift &")
--    spawnOnce "blueberry-tray"
    spawnOnce "protonmail-bridge --noninteractive"

    -- Applications
    spawnOnce "steam -nochatui -nofriendsui -nonewsui -silent"
--    spawnOnce "~/Applications/net.lutris.battlenet-1.desktop"
--    spawnOnce "weakauras-companion"
--    spawnOnce "~/Applications/net.lutris.tsm-2.desktop"
    spawnOnce "megasync"
    spawnOnce "mailspring"
    spawnOnce "atomic-tweetdeck"
    spawnOnce "qpwgraph -m"
    spawnOnce "signal-desktop"
    spawnOnce "discord"
    spawnOnce ("sleep 5 && kitty -T=taskManager --override background_opacity=1 --override font_size=10 btop")
    spawnOnce "emacsclient -c -a 'emacs' '~/Documents/MEGAsync/org/agenda.org'"
    spawnOnce "qutebrowser -r default"
--    spawnOnce myFrontCam
--    spawnOnce myBackCam
    spawnOnce "barrier"

    --spawnOnce "xargs xwallpaper --stretch < ~/.xwallpaper"  -- set last saved with xwallpaper
    -- spawnOnce "/bin/ls ~/wallpapers | shuf -n 1 | xargs xwallpaper --stretch"  -- set random xwallpaper
    -- spawnOnce "~/.fehbg &"  -- set last saved feh wallpaper
--    spawnOnce "nitrogen --restore &"   -- if you prefer nitrogen to feh
    spawnOnce "/drives/tbSSD/Applications/Cloning/styli.sh/styli.sh -w 2560 -h 1440 -s nature"
--    spawnOnce "feh --randomize --bg-fill /drives/tbSSD/Stuff/Wallpapers/*"  -- feh set random wallpaper
    setWMName "LG3D"

myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
                  (0x28,0x2c,0x34) -- lowest inactive bg
                  (0x28,0x2c,0x34) -- highest inactive bg
                  (0xc7,0x92,0xea) -- active bg
                  (0xc0,0xa7,0x9a) -- inactive fg
                  (0x28,0x2c,0x34) -- active fg

-- gridSelect menu layout
mygridConfig :: p -> GSConfig Window
mygridConfig colorizer = (buildDefaultGSConfig myColorizer)
    { gs_cellheight   = 40
    , gs_cellwidth    = 150
    , gs_cellpadding  = 6
    , gs_originFractX = 0.5
    , gs_originFractY = 0.5
    , gs_font         = myFont
    }

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = def
                   { gs_cellheight   = 40
                   , gs_cellwidth    = 150
                   , gs_cellpadding  = 6
                   , gs_originFractX = 0.5
                   , gs_originFractY = 0.5
                   , gs_font         = myFont
                   }

myAppGrid = [ 
                 ("Steam", "mangohud steam -nochatui -nofriendsui -nonewsui")
                 , ("LibreOffice Writer", "lowriter")
                 , ("Battlenet", "~/Applications/net.lutris.battlenet-1.desktop")
                 , ("TSM", "~/Applications/net.lutris.tsm-2.desktop")
                 , ("Matlab", (myTerminal ++ " devour /drives/tbSSD/Applications/Matlab/bin/matlab"))
                 , ("Elden Ring", "net.lutris.elden-ring-10.desktop")
                 , ("God of War", "net.lutris.god-of-war-7.desktop")
                 , ("ProtonUp", "ProtonUp-Qt-2.4.1-x86_64.AppImage")
                 ]

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "calculator" spawnCalc findCalc manageCalc
                , NS "msp" spawnMsp findMsp manageMsp
                ]
  where
    spawnTerm  = "kitty -T=scratchpad --override background_opacity=1"
    findTerm   = title =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.5
                 w = 0.9
                 t = 0.5 -h
                 l = 0.95 -w
    spawnMsp   = "kitty -T=volumeControl --override background_opacity=1 --override font_size=10 pulsemixer"
    findMsp    = title =? "volumeControl"
    manageMsp  = customFloating $ W.RationalRect l t w h
               where
                 h = 0.5
                 w = 0.9
                 t = 0.5 -h
                 l = 0.95 -w
    spawnCalc  = "qalculate-gtk"
    findCalc   = className =? "Qalculate-gtk"
    manageCalc = customFloating $ W.RationalRect l t w h
               where
                 h = 0.5
                 w = 0.4
                 t = 0.75 -h
                 l = 0.70 -w

--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.
tall     = renamed [Replace "tall"]
           $ smartBorders
           $ limitWindows 12
           $ mySpacing 6
           $ ResizableTall 1 (3/100) (1/2) []
magnify  = renamed [Replace "magnify"]
--           $ smartBorders
           $ magnifier
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
monocle  = renamed [Replace "monocle"]
           $ smartBorders
           $ limitWindows 20 Full
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ smartBorders
           $ limitWindows 12
           $ mySpacing 0
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
--spirals  = renamed [Replace "spirals"]
--           $ smartBorders
 --          $ subLayout [] (smartBorders Simplest)
 --          $ mySpacing 6
 --          $ spiral (6/7)
-- threeCol = renamed [Replace "threeCol"]
--           $ smartBorders
--           $ subLayout [] (smartBorders Simplest)
--           $ limitWindows 7
--           $ ThreeCol 1 (3/100) (1/2)
wide= renamed [Replace "wide"]
           $ smartBorders
           $ mySpacing 6
           $ limitWindows 12
           -- Mirror takes a layout and rotates it by 90 degrees.
           -- So we are applying Mirror to the ThreeCol layout.
           $ Mirror
           $ ResizableTall 1 (3/100) (1/2) []
--tabs     = renamed [Replace "tabs"]
           -- I cannot add spacing to this layout because it will
           -- add spacing between window and tabs which looks bad.
--           $ tabbed shrinkText myTabTheme
tallAccordion  = renamed [Replace "tallAccordion"]
           $ Accordion
wideAccordion  = renamed [Replace "wideAccordion"]
           $ Mirror Accordion

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = color07
                 , inactiveColor       = color02
                 , activeBorderColor   = color15
                 , inactiveBorderColor = colorBack
                 , activeTextColor     = color09
                 , inactiveTextColor   = color14
                 }

-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Ubuntu:bold:size=60"
    , swn_fade              = 1.5
    , swn_bgcolor           = "#282a36  "
    , swn_color             = "#8be9fd"
    }

-- The layout hook
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     tall
--                                 ||| noBorders tabs
--                                 ||| floats
                                 ||| wide
--                                 ||| spirals
--                                 ||| grid
                                 ||| noBorders monocle
--                                 ||| threeCol
--                                 ||| threeRow
--                                 ||| tallAccordion
--                                 ||| wideAccordion

-- myWorkspaces = [" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 "]
myWorkspaces = [" main ", " off ", " max ", " chat ", " sys ", " launcher ", " bkgrd "]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..] -- (,) == \x y -> (x,y)

clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
    where i = fromJust $ M.lookup ws myWorkspaceIndices

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     -- 'doFloat' forces a window to float.  Useful for dialog boxes and such.
     -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
     -- I'm doing it this way because otherwise I would have to write out the full
     -- name of my workspaces and the names would be very long if using clickable workspaces.
     [ className =? "confirm"         --> doFloat
     , className =? "file_progress"   --> doFloat
     , className =? "dialog"          --> doFloat
     , className =? "download"        --> doFloat
     , className =? "error"           --> doFloat
     , className =? "Gimp"            --> doFloat
     , className =? "notification"    --> doFloat
     , className =? "pinentry-gtk-2"  --> doFloat
     , className =? "splash"          --> doFloat
     , className =? "toolbar"         --> doFloat
     , className =? "MEGAsync"        --> doFloat
     , className =? "Yad"             --> doCenterFloat
     , title =? "File Operation Progress"         --> doCenterFloat
     , title =? "Arcolinux Logout"         --> doCenterFloat
     , className =? "Sxiv"         --> doFloat
     , className =? "Nsxiv"         --> doFloat
     , className =? "feh"         --> doFloat
     , title =? "Oracle VM VirtualBox Manager"  --> doFloat
--     , className =? "MEGAsync"   --> doShift ( myWorkspaces !! 6 )
     , className =? "ProtonMail Bridge"   --> doShift ( myWorkspaces !! 6 )
     , title =? "taskManager"   --> doShift ( myWorkspaces !! 4 )
     , className =? "openrgb"   --> doShift ( myWorkspaces !! 6 )
     , className =? "discord"   --> doShift ( myWorkspaces !! 3 )
     , className =? "Atomic TweetDeck"   --> doShift ( myWorkspaces !! 1 )
     , className =? "Mailspring"   --> doShift ( myWorkspaces !! 3 )
     , className =? "signal"   --> doShift ( myWorkspaces !! 1 )
     , className =? "Signal"   --> doShift ( myWorkspaces !! 1 )
     , className =? "qutebrowser"   --> doShift ( myWorkspaces !! 0 )
     , className =? "wow.exe"     --> doShift ( myWorkspaces !! 2 )
     , className =? "fusion360.exe"     --> doShift ( myWorkspaces !! 2 )
     , className =? "Emacs"     --> doShift ( myWorkspaces !! 0 )
     , title =? "Media Server - mpv"     --> doShift ( myWorkspaces !! 1 )
     , title =? "World of Warcraft"     --> doShift ( myWorkspaces !! 2 )
     , title =? "steam_app_1593500"     --> doShift ( myWorkspaces !! 2 )
     , title =? "Battle.net"     --> doShift ( myWorkspaces !! 5 )
     , title =? "Battle.net Login"     --> doShift ( myWorkspaces !! 5 )
     , className =? "lutris"     --> doShift ( myWorkspaces !! 5 )
     , title =? "Steam"     --> doShift ( myWorkspaces !! 5 )
     , className =? "Lutris"     --> doShift ( myWorkspaces !! 5 )
     , className =? "VirtualBox Manager" --> doShift  ( myWorkspaces !! 2 )
     , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
     , (className =? "brave" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog

      -- OBS
     , title =? "Chat"         --> doShift ( myWorkspaces !! 1 )
     , title =? "Stream Information"    --> doShift ( myWorkspaces !! 4 )
     , title =? "OBS 27.2.1 (linux) - Profile: Untitled - Scenes: Untitled"    --> doShift ( myWorkspaces !! 4 )
--     , isFullscreen -->  doFullFloat
     ] <+> namedScratchpadManageHook myScratchPads

subtitle' ::  String -> ((KeyMask, KeySym), NamedAction)
subtitle' x = ((0,0), NamedAction $ map toUpper
                      $ sep ++ "\n-- " ++ x ++ " --\n" ++ sep)
  where
    sep = replicate (6 + length x) '-'

showKeybindings :: [((KeyMask, KeySym), NamedAction)] -> NamedAction
showKeybindings x = addName "Show Keybindings" $ io $ do
  h <- spawnPipe $ "yad --text-info --fontname=\"SauceCodePro Nerd Font Mono 12\" --fore=#46d9ff back=#282c36 --center --geometry=1200x800 --title \"XMonad keybindings\""
  --hPutStr h (unlines $ showKm x) -- showKM adds ">>" before subtitles
  hPutStr h (unlines $ showKmSimple x) -- showKmSimple doesn't add ">>" to subtitles
  hClose h
  return ()

myKeys :: XConfig l0 -> [((KeyMask, KeySym), NamedAction)]
myKeys c =
  --(subtitle "Custom Keys":) $ mkNamedKeymap c $
  let subKeys str ks = subtitle' str : mkNamedKeymap c ks in
  subKeys "Xmonad Essentials"
    -- Xmonad
        [ ("M-C-r", addName "Recompile Xmonad"          $ spawn "xmonad --recompile")
        , ("M-S-r", addName "Restart Xmonad"            $ spawn "xmonad --restart")
        , ("M-S-c", addName "Kill focused window"       $ kill1)
        , ("M-z", addName "Sink all windows"            $ sinkAll)


    -- Run Prompt
        , ("M-S-g", addName "dmenu"                     $ spawn "dmenu_run -i -m 1")
        , ("M-S-u", addName "update configs"            $ spawn "~/.config/dmenu/configsgitlab.sh")
        , ("M-S-<Return>", addName "Rofi"               $ spawn "rofi -show run")

    -- Applications
        , ("M-S-i", addName "Brave"                     $ spawn "brave")
        , ("M-S-q", addName "Qutebrowser"               $ spawn "qutebrowser -r default")
        , ("M-S-l", addName "Lutris"                    $ spawn "lutris")
        , ("M-S-d", addName "Discord"                   $ spawn "discord")
        , ("M-S-f", addName "File Explorer"             $ spawn "pcmanfm-qt")
        , ("M-C-p", addName "Volume Mixer - GUI"        $ spawn "pavucontrol")
        , ("M-S-v", addName "VLC"                       $ spawn "vlc")
        , ("M-S-s", addName "Screenshot"                $ spawn "flameshot gui")
        , ("C-M1-<Backspace>", addName "Logout Menu"    $ spawn "arcolinux-logout")
        , ("M-S-m", addName "Matlab"                    $ spawn (myTerminal ++ " devour /drives/tbSSD/Applications/Matlab/bin/matlab"))

                        -- Other Dmenu Prompts
    -- In Xmonad and many tiling window managers, M-p is the default keybinding to
    -- launch dmenu_run, so I've decided to use M-p plus KEY for these dmenu scripts.
        , ("M-p", addName "Passwords"                   $ spawn "keepmenu")

    -- Useful programs to have a keybinding for launch
        , ("M-<Return>", addName "Terminal"             $ spawn (myTerminal))
        , ("M-S-h", addName "Task Manager"              $ spawn ("kitty -T=taskManager --override background_opacity=1 --override font_size=10 btop"))

    -- Increase/decrease spacing (gaps)
        , ("C-M1-j", addName "Decrease window space"    $ decWindowSpacing 4)
        , ("C-M1-k", addName "Decrease window space"    $ incWindowSpacing 4)
        , ("C-M1-h", addName "Decrease window space"    $ decScreenSpacing 4)
        , ("C-M1-l", addName "Increase window space"    $ incScreenSpacing 4)
        , ("M-h", addName "-x window space"             $ sendMessage Shrink)
        , ("M-l", addName "+x window space"             $ sendMessage Expand)
        , ("M-M1-j", addName "-y window space"          $ sendMessage MirrorShrink)
        , ("M-M1-k", addName "+y window space"          $ sendMessage MirrorExpand)]

        ^++^ subKeys "Switch to workspace"
        [ ("M-1", addName "Switch to workspace 1"       $ (windows $ W.greedyView $ myWorkspaces !! 0))
        , ("M-2", addName "Switch to workspace 2"       $ (windows $ W.greedyView $ myWorkspaces !! 1))
        , ("M-3", addName "Switch to workspace 3"       $ (windows $ W.greedyView $ myWorkspaces !! 2))
        , ("M-4", addName "Switch to workspace 4"       $ (windows $ W.greedyView $ myWorkspaces !! 3))
        , ("M-5", addName "Switch to workspace 5"       $ (windows $ W.greedyView $ myWorkspaces !! 4))
        , ("M-6", addName "Switch to workspace 6"       $ (windows $ W.greedyView $ myWorkspaces !! 5))
        , ("M-7", addName "Switch to workspace 7"       $ (windows $ W.greedyView $ myWorkspaces !! 6))
        , ("M-8", addName "Switch to workspace 8"       $ (windows $ W.greedyView $ myWorkspaces !! 7))
        , ("M-9", addName "Switch to workspace 9"       $ (windows $ W.greedyView $ myWorkspaces !! 8))]

        ^++^ subKeys "Send window to workspace"
        [ ("M-S-1", addName "Send to workspace 1"       $ (windows $ W.shift $ myWorkspaces !! 0))
        , ("M-S-2", addName "Send to workspace 2"       $ (windows $ W.shift $ myWorkspaces !! 1))
        , ("M-S-3", addName "Send to workspace 3"       $ (windows $ W.shift $ myWorkspaces !! 2))
        , ("M-S-4", addName "Send to workspace 4"       $ (windows $ W.shift $ myWorkspaces !! 3))
        , ("M-S-5", addName "Send to workspace 5"       $ (windows $ W.shift $ myWorkspaces !! 4))
        , ("M-S-6", addName "Send to workspace 6"       $ (windows $ W.shift $ myWorkspaces !! 5))
        , ("M-S-7", addName "Send to workspace 7"       $ (windows $ W.shift $ myWorkspaces !! 6))
        , ("M-S-8", addName "Send to workspace 8"       $ (windows $ W.shift $ myWorkspaces !! 7))
        , ("M-S-9", addName "Send to workspace 9"       $ (windows $ W.shift $ myWorkspaces !! 8))]
        ^++^ subKeys "Monitors"
        [ ("M-e", addName "Switch focus to next monitor" $ viewScreen def 1)
        , ("M-w", addName "Switch focus to prev monitor" $ viewScreen def 0)


    -- Grid Select (CTR-g followed by a key)
        , ("C-g", addName "Select Favorite apps"        $ spawnSelected' myAppGrid)

    -- Windows navigation
        --, ("M-m", addName "Enlage focused window"                  $ windows W.focusMaster)
        , ("M-j", addName "Change focus down"           $ windows W.focusDown)
        , ("M-<Tab>", addName "Change focus down"       $ windows W.focusDown)
        , ("M-k", addName "Change focus up"             $ windows W.focusUp)
        , ("M-S-j", addName "Move focus down"           $ windows W.swapDown)
        , ("M-S-k", addName "Move focus up"             $ windows W.swapUp)
        --, ("M-<Backspace>", addName "zz"                  $ promote)

    -- Layouts
        , ("M-,", addName "Next Layout"                 $ sendMessage NextLayout)
        , ("M-<Space>", addName "Toggle fullscreen"     $ sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts)

    -- Increase/decrease windows in the master pane or the stack
        , ("M-C-=", addName "Increase number in master" $ sendMessage (IncMasterN 1))
        , ("M-C--", addName "Decrease number in master" $ sendMessage (IncMasterN (-1)))

    -- Scratchpads
    -- Toggle show/hide these programs.  They run on a hidden workspace.
    -- When you toggle them to show, it brings them to your current workspace.
    -- Toggle them to hide and it sends them back to hidden workspace (NSP).
        , ("M-C-<Return>", addName "Scratchpad Terminal"$ namedScratchpadAction myScratchPads "terminal")
        , ("M-S-p", addName "Scratchpad Volume"         $ namedScratchpadAction myScratchPads "msp")
        , ("M-C-c", addName "Scratchpad Calculator"     $ namedScratchpadAction myScratchPads "calculator")

    -- Emacs (CTRL-e followed by a key)
        , ("C-e e", addName "Emacsclient"               $ spawn (myEmacs ++ ("--eval '(dashboard-refresh-buffer)'")))   -- emacs dashboard
        , ("C-e b", addName "Emacs Buffer"              $ spawn (myEmacs ++ ("--eval '(ibuffer)'")))
        , ("C-e d", addName "Emacs Dired"               $ spawn (myEmacs ++ ("--eval '(dired nil)'")))
        , ("C-e f", addName "Emacs Elfeed"              $ spawn (myEmacs ++ ("--eval '(elfeed)'")))
        , ("C-e s", addName "Emacs Terminal"            $ spawn (myEmacs ++ ("--eval '(vterm)'")))
        , ("C-e x", addName "Emacs xmonad"              $ spawn (myEmacs ++ ("~/.config/xmonad/xmonad.org")))
        , ("C-e a", addName "Emacs Agenda"              $ spawn (myEmacs ++ ("~/Documents/MEGAsync/org/agenda.org")))

    -- Multimedia Keys
        , ("<XF86AudioMute>", addName "mute"
            $ spawn "amixer set Master mute; dunstify -i $icondSound -r \"Volume muted\" -u normal ")
        , ("<XF86AudioLowerVolume>", addName "Volume -"
            $ spawn "amixer set Master 5%- unmute; dunstify -i $icondSound -r \"Volume down 5%\" -u normal ")
        , ("<XF86AudioRaiseVolume>", addName "Volume +"
            $ spawn "amixer set Master 5%+ unmute; dunstify -i $icondSound -r \"Volume up 5%\" -u normal ")
        , ("<XF86Calculator>", addName "Scratchpad Calculator"
            $ runOrRaise "qalculate-gtk" (resource =? "qalculate-gtk"))
        ]
    -- The following lines are needed for named scratchpads.
          where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))

main :: IO ()
main = do
    -- Launching three instances of xmobar on their monitors.
    xmproc0 <- spawnPipe ("xmobar -x 0 $HOME/.config/xmobar/" ++ colorScheme ++ "-xmobarrc0")
    xmproc1 <- spawnPipe ("xmobar -x 1 $HOME/.config/xmobar/" ++ colorScheme ++ "-xmobarrc2")
    -- the xmonad, ya know...what the WM is named after!
    xmonad $ addDescrKeys' ((mod4Mask, xK_F1), showKeybindings) myKeys $ ewmh $ docks $ def
        { manageHook         = myManageHook <+> manageDocks
        --, handleEventHook    = docksEventHook
                               -- Uncomment this line to enable fullscreen support on things like YouTube/Netflix.
                               -- This works perfect on SINGLE monitor systems. On multi-monitor systems,
                               -- it adds a border around the window if screen does not have focus. So, my solution
                               -- is to use a keybinding to toggle fullscreen noborders instead.  (M-<Space>)
                               -- <+> fullscreenEventHook
        , modMask            = myModMask
        , terminal           = myTerminal
        , startupHook        = myStartupHook
        , layoutHook         = showWName' myShowWNameTheme $ myLayoutHook
        , workspaces         = myWorkspaces
        , borderWidth        = myBorderWidth
        , normalBorderColor  = myNormColor
        , focusedBorderColor = myFocusColor
        , logHook = dynamicLogWithPP $  filterOutWsPP [scratchpadWorkspaceTag] $ xmobarPP
          { ppOutput = \x -> hPutStrLn xmproc0 x   -- xmobar on monitor 1
                        >> hPutStrLn xmproc1 x   -- xmobar on monitor 2
          , ppCurrent = xmobarColor color03 "" . wrap
                      ("<box type=Bottom width=2 mb=2 color=" ++ color03 ++ ">") "</box>"
          --             "[" "]"           -- Current workspace
          -- Visible but not current workspace
          , ppVisible = xmobarColor color03 "" . clickable
          -- Hidden workspace
          , ppHidden = xmobarColor color06 "" . wrap
          --             "*" "" . clickable
                     ("<box type=Top width=2 mt=2 color=" ++ color06 ++ ">") "</box>" . clickable
          -- Hidden workspaces (no windows)
          , ppHiddenNoWindows = xmobarColor color05 ""  . clickable
          -- Title of active window
          , ppTitle = xmobarColor color16 "" . shorten 60
          -- Separator character
          , ppSep =  "<fc=" ++ color09 ++ "> <fn=1>|</fn> </fc>"
          -- Urgent workspace
          , ppUrgent = xmobarColor color02 "" . wrap "!" "!"
          -- Adding # of windows on current workspace to the bar
          , ppExtras  = [windowCount]
          -- order of things in xmobar
          , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
          }
    }
